#include <iostream>
#include <srrg_hbst/types/binary_tree.hpp>

// ds we associate our data with integer indexes (uint64_t)
typedef srrg_hbst::BinaryTree256<uint64_t> Tree;

// ds dummy descriptor generation
Tree::MatchableVector getDummyMatchables(const uint64_t& number_of_matchables_);

int32_t main() {
  // ds train descriptor pool
  const Tree::MatchableVector matchables_reference = std::move(getDummyMatchables(10000));

  // ds allocate a BTree object on these descriptors (no shared pointer passed as the tree will have
  // its own constant copy of the train descriptors)
  Tree hbst_tree(0, matchables_reference);

  // ds query descriptor pool
  const Tree::MatchableVector matchables_query = std::move(getDummyMatchables(5000));

  // ds get matches (opencv IN/OUT style)
  Tree::MatchVector matches1;
  hbst_tree.matchLazy(matchables_query, matches1);

  // ds get matches directly
  const std::shared_ptr<const Tree::MatchVector> matches2 =
    hbst_tree.getMatchesLazy(std::make_shared<const Tree::MatchableVector>(matchables_query));

  // ds match count functions (FAST)
  const uint64_t number_of_matches      = hbst_tree.getNumberOfMatches(matchables_query);
  const double matching_ratio           = hbst_tree.getMatchingRatio(matchables_query);
  const uint64_t number_of_matches_lazy = hbst_tree.getNumberOfMatchesLazy(matchables_query);
  const double matching_ratio_lazy      = hbst_tree.getMatchingRatioLazy(matchables_query);

  std::cerr << "matches: " << number_of_matches << "/" << matchables_query.size() << std::endl;
  std::cerr << "  ratio: " << matching_ratio << std::endl;
  std::cerr << "lazy matches: " << number_of_matches_lazy << "/" << matchables_query.size()
            << std::endl;
  std::cerr << "       ratio: " << matching_ratio_lazy << std::endl;

  // ds matches must be identical: check if number of elements differ
  if (matches1.size() != matches2->size()) {
    std::cerr << "received inconsistent matching returns" << std::endl;
    return -1;
  }

  // ds check each element
  for (uint64_t index_match = 0; index_match < matches1.size(); ++index_match) {
    // ds check if not matching
    if (matches1[index_match].object_query != matches2->at(index_match).object_query ||
        matches1[index_match].object_references.size() !=
          matches2->at(index_match).object_references.size() ||
        matches1[index_match].object_references[0] !=
          matches2->at(index_match).object_references[0] ||
        matches1[index_match].distance != matches2->at(index_match).distance) {
      std::cerr << "received inconsistent matching returns" << std::endl;
      return -1;
    }
  }

  // ds clear query matchables (not stored in the tree)
  for (const Tree::Matchable* matchable : matchables_query) {
    delete matchable;
  }

  // ds clear matchables stored in the tree
  hbst_tree.clear(true);

  // ds done
  std::cout << "successfully matched descriptors: " << matches1.size() << std::endl;
  return 0;
}

Tree::MatchableVector getDummyMatchables(const uint64_t& number_of_matchables_) {
  // ds preallocate vector
  Tree::MatchableVector matchables(number_of_matchables_);

  // ds set values
  for (uint64_t identifier = 0; identifier < number_of_matchables_; ++identifier) {
    // ds generate a "random" descriptor by flipping some bits
    Tree::Matchable::Descriptor descriptor;
    for (uint32_t u = 0; u < 256 /*descriptor size in bits*/ / 3; ++u) {
      if (rand() % 2) {
        descriptor.flip(u);
      }
    }

    // ds set matchable
    matchables[identifier] = new Tree::Matchable(identifier, descriptor);
  }

  // ds done
  return matchables;
}
